package ru.t1.ktitov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<SessionDTO> criteriaQuery = criteriaBuilder.createQuery(SessionDTO.class);
        Root<SessionDTO> session = criteriaQuery.from(SessionDTO.class);
        criteriaQuery.select(session)
                .where(criteriaBuilder.equal(session.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(session.get(Sort.getOrderByField(sort))));
        TypedQuery<SessionDTO> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public SessionDTO findOneById(@Nullable final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public void clear() {
        @Nullable final List<SessionDTO> sessions = findAll();
        if (!sessions.isEmpty())
            sessions.forEach(entityManager::remove);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
