package ru.t1.ktitov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.model.IProjectRepository;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.model.Project;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final String jpql = "SELECT m FROM Project m";
        return entityManager.createQuery(jpql, Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Project> criteriaQuery = criteriaBuilder.createQuery(Project.class);
        Root<Project> project = criteriaQuery.from(Project.class);
        criteriaQuery.select(project)
                .where(criteriaBuilder.equal(project.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(project.get(Sort.getOrderByField(sort))));
        TypedQuery<Project> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public Project findOneById(@Nullable final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE id = :id AND user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project";
        return entityManager.createQuery(jpql, Project.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public void clear() {
        @Nullable final List<Project> projects = findAll();
        if (!projects.isEmpty())
            projects.forEach(entityManager::remove);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = "DELETE FROM Project m WHERE user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
