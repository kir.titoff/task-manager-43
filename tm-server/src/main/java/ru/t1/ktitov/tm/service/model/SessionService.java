package ru.t1.ktitov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.model.ISessionRepository;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.model.ISessionService;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository>
        implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

}
