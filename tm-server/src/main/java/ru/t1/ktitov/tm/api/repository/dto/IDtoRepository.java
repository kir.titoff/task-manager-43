package ru.t1.ktitov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {

    @Nullable
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    M update(@NotNull M model);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    M findOneById(@Nullable String id);

    boolean existsById(@Nullable String id);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    void clear();

}
