/*
package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.model.IProjectService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.service.dto.ProjectService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService service = new ProjectService(connectionService);

    private void compareProjects(@NotNull final ProjectDTO project1, @NotNull final ProjectDTO project2) {
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project1.getDescription(), project2.getDescription());
        Assert.assertEquals(project1.getStatus(), project2.getStatus());
        Assert.assertEquals(project1.getUserId(), project2.getUserId());
        Assert.assertEquals(project1.getCreated(), project2.getCreated());
    }

    private void compareProjects(
            @NotNull final List<ProjectDTO> projectList1,
            @NotNull final List<ProjectDTO> projectList2) {
        Assert.assertEquals(projectList1.size(), projectList2.size());
        for (int i = 0; i < projectList1.size(); i++) {
            compareProjects(projectList1.get(i), projectList2.get(i));
        }
    }

    @After
    public void tearDown() {
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER1_PROJECT1);
        service.add(USER2_PROJECT1);
        compareProjects(USER1_PROJECT1, service.findAll().get(0));
        compareProjects(USER2_PROJECT1, service.findAll().get(1));
    }

    @Test
    public void addByUserId() {
        service.add(USER1.getId(), USER1_PROJECT1);
        compareProjects(USER1_PROJECT1, service.findAll().get(0));
        Assert.assertEquals(USER1.getId(), service.findAll().get(0).getUserId());
    }

    @Test
    public void addList() {
        service.add(USER1_PROJECT_LIST);
        Assert.assertEquals(3, service.getSize());
        compareProjects(USER1_PROJECT1, service.findAll().get(0));
        compareProjects(USER1_PROJECT2, service.findAll().get(1));
        compareProjects(USER1_PROJECT3, service.findAll().get(2));
    }

    @Test
    public void setList() {
        service.add(USER1_PROJECT_LIST);
        service.set(USER2_PROJECT_LIST);
        compareProjects(USER2_PROJECT1, service.findAll().get(0));
    }

    @Test
    public void clear() {
        service.add(USER1_PROJECT_LIST);
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
        service.add(USER1_PROJECT_LIST);
        service.add(USER2_PROJECT_LIST);
        service.clear(USER1.getId());
        Assert.assertEquals(1, service.getSize());
        compareProjects(USER2_PROJECT1, service.findAll().get(0));
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1_PROJECT_LIST);
        service.add(USER2_PROJECT_LIST);
        service.add(ADMIN1_PROJECT_LIST);
        compareProjects(USER1_PROJECT_LIST, service.findAll(USER1.getId()));
        compareProjects(USER2_PROJECT_LIST, service.findAll(USER2.getId()));
        compareProjects(ADMIN1_PROJECT_LIST, service.findAll(ADMIN3.getId()));
    }

    @Test
    public void existsById() {
        service.add(USER1_PROJECT1);
        service.add(USER2_PROJECT1);
        Assert.assertTrue(service.existsById(USER1_PROJECT1.getId()));
        Assert.assertTrue(service.existsById(USER2_PROJECT1.getId()));
        Assert.assertFalse(service.existsById(USER1_PROJECT2.getId()));
        Assert.assertTrue(service.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(service.existsById(USER1.getId(), USER2_PROJECT1.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER1_PROJECT1);
        service.add(USER2_PROJECT1);
        compareProjects(USER1_PROJECT1, service.findOneById(USER1_PROJECT1.getId()));
        compareProjects(USER2_PROJECT1, service.findOneById(USER2_PROJECT1.getId()));
        compareProjects(USER1_PROJECT1, service.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
        thrown.expect(EntityNotFoundException.class);
        compareProjects(USER1_PROJECT2, service.findOneById(USER1_PROJECT2.getId()));
        compareProjects(USER2_PROJECT1, service.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
    }

    @Test
    public void remove() {
        service.add(USER1_PROJECT_LIST);
        service.add(USER2_PROJECT_LIST);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1_PROJECT1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER1_PROJECT2.getId());
        Assert.assertEquals(2, service.getSize());
        compareProjects(USER1_PROJECT3, service.findAll().get(0));
        compareProjects(USER2_PROJECT1, service.findAll().get(1));
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeByUserId() {
        service.add(USER1_PROJECT_LIST);
        service.add(USER2_PROJECT_LIST);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER1.getId(), USER1_PROJECT2.getId());
        Assert.assertEquals(2, service.getSize());
        thrown.expect(EntityNotFoundException.class);
        service.remove(USER2.getId(), USER1_PROJECT3);
        Assert.assertEquals(2, service.getSize());
        compareProjects(USER1_PROJECT3, service.findAll().get(0));
        compareProjects(USER2_PROJECT1, service.findAll().get(1));
    }

    @Test
    public void create() {
        service.create(USER2.getId(), "project-2", "description of project 2");
        Assert.assertEquals(1, service.getSize());
        Assert.assertEquals("project-2", service.findAll().get(0).getName());
        Assert.assertEquals("description of project 2", service.findAll().get(0).getDescription());
        Assert.assertEquals(Status.NOT_STARTED, service.findAll().get(0).getStatus());
    }

    @Test
    public void updateById() {
        @NotNull final ProjectDTO project = service.create(USER1.getId(),
                "project-4", "description of project 4");
        service.updateById(USER1.getId(), project.getId(),
                "upd-project-4", "upd description of project 4");
        @NotNull final ProjectDTO foundProject = service.findOneById(project.getId());
        Assert.assertEquals("upd-project-4", foundProject.getName());
        Assert.assertEquals("upd description of project 4", foundProject.getDescription());
    }

    @Test
    public void changeProjectStatusById() {
        service.add(USER1_PROJECT1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_PROJECT1.getStatus());
        service.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
        @NotNull final ProjectDTO foundProject = service.findOneById(USER1_PROJECT1.getId());
        Assert.assertEquals(Status.IN_PROGRESS, foundProject.getStatus());
    }

}
*/
