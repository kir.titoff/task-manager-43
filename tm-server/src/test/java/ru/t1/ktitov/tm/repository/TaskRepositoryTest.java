/*
package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.repository.dto.TaskRepositoryDTO;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected ITaskRepositoryDTO getRepository(@NotNull EntityManager entityManager) {
        return new TaskRepositoryDTO(entityManager);
    }

    private void compareTasks(@NotNull final TaskDTO task1, @NotNull final TaskDTO task2) {
        Assert.assertEquals(task1.getId(), task2.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task1.getDescription(), task2.getDescription());
        Assert.assertEquals(task1.getStatus(), task2.getStatus());
        Assert.assertEquals(task1.getUserId(), task2.getUserId());
        Assert.assertEquals(task1.getProjectId(), task2.getProjectId());
        Assert.assertEquals(task1.getCreated(), task2.getCreated());
    }

    private void compareTasks(
            @NotNull final List<TaskDTO> taskList1,
            @NotNull final List<TaskDTO> taskList2) {
        Assert.assertEquals(taskList1.size(), taskList2.size());
        for (int i = 0; i < taskList1.size(); i++) {
            compareTasks(taskList1.get(i), taskList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER1.getId());
            repository.clear(USER2.getId());
            repository.clear(ADMIN3.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            TaskDTO task = repository.findAll().get(0);
            compareTasks(USER1_TASK1, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            Assert.assertEquals(3, repository.getSize());
            compareTasks(USER1_TASK1, repository.findAll().get(0));
            compareTasks(USER1_TASK2, repository.findAll().get(1));
            compareTasks(USER1_TASK3, repository.findAll().get(2));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            compareTasks(USER1_TASK1, repository.findAll().get(0));
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.clear(USER2.getId());
            Assert.assertFalse(repository.findAll().isEmpty());
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            repository.clear(USER2.getId());
            compareTasks(USER1_TASK1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.add(USER2_TASK1);
            compareTasks(USER1_TASK_LIST, repository.findAll(USER1.getId()));
            compareTasks(USER2_TASK_LIST, repository.findAll(USER2.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            compareTasks(USER1_TASK1, repository.findOneById(USER1_TASK1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId(), USER1_TASK1.getId()));
            compareTasks(USER1_TASK1, repository.findOneById(USER1.getId(), USER1_TASK1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.remove(USER1_TASK1);
            Assert.assertEquals(2, repository.getSize());
            repository.removeById(USER1_TASK2.getId());
            Assert.assertEquals(1, repository.getSize());
            compareTasks(USER1_TASK3, repository.findAll().get(0));
            repository.clear();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            Assert.assertEquals(3, repository.getSize());
            repository.removeById(USER2.getId(), USER1_TASK1.getId());
            Assert.assertEquals(3, repository.getSize());
            repository.removeById(USER1.getId(), USER1_TASK1.getId());
            repository.removeById(USER1.getId(), USER1_TASK2.getId());
            compareTasks(USER1_TASK3, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.add(USER2_TASK1);
            compareTasks(USER1_TASK_LIST, repository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
*/
