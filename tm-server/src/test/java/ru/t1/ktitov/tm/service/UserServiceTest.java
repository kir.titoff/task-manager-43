/*
package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.ktitov.tm.api.service.*;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.IProjectService;
import ru.t1.ktitov.tm.api.service.model.ITaskService;
import ru.t1.ktitov.tm.api.service.model.IUserService;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.user.UserLoginExistsException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.dto.model.UserDTO;
import ru.t1.ktitov.tm.service.dto.ProjectService;
import ru.t1.ktitov.tm.service.dto.TaskService;
import ru.t1.ktitov.tm.service.dto.UserService;
import ru.t1.ktitov.tm.util.HashUtil;

import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService service = new UserService(connectionService, propertyService);

    private void compareUsers(@NotNull final UserDTO user1, @NotNull final UserDTO user2) {
        Assert.assertEquals(user1.getId(), user2.getId());
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getEmail(), user2.getEmail());
        Assert.assertEquals(user1.getFirstName(), user2.getFirstName());
        Assert.assertEquals(user1.getLastName(), user2.getLastName());
        Assert.assertEquals(user1.getMiddleName(), user2.getMiddleName());
        Assert.assertEquals(user1.getRole(), user2.getRole());
        Assert.assertEquals(user1.getLocked(), user2.getLocked());
    }

    private void compareUsers(
            @NotNull final List<UserDTO> userList1,
            @NotNull final List<UserDTO> userList2) {
        Assert.assertEquals(userList1.size(), userList2.size());
        for (int i = 0; i < userList1.size(); i++) {
            compareUsers(userList1.get(i), userList2.get(i));
        }
    }

    private void compareProjects(@NotNull final ProjectDTO project1, @NotNull final ProjectDTO project2) {
        Assert.assertEquals(project1.getId(), project2.getId());
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project1.getDescription(), project2.getDescription());
        Assert.assertEquals(project1.getStatus(), project2.getStatus());
        Assert.assertEquals(project1.getUserId(), project2.getUserId());
        Assert.assertEquals(project1.getCreated(), project2.getCreated());
    }

    private void compareTasks(@NotNull final TaskDTO task1, @NotNull final TaskDTO task2) {
        Assert.assertEquals(task1.getId(), task2.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task1.getDescription(), task2.getDescription());
        Assert.assertEquals(task1.getStatus(), task2.getStatus());
        Assert.assertEquals(task1.getUserId(), task2.getUserId());
        Assert.assertEquals(task1.getProjectId(), task2.getProjectId());
        Assert.assertEquals(task1.getCreated(), task2.getCreated());
    }

    @After
    public void tearDown() {
        try {
            service.removeByLogin("user1");
        } catch (final Exception e) {
        }
        try {
            service.removeByLogin("user2");
        } catch (final Exception e) {
        }
        try {
            service.removeByLogin("user3");
        } catch (final Exception e) {
        }
        try {
            service.removeByLogin("user4");
        } catch (final Exception e) {
        }
        try {
            service.removeByLogin("test");
        } catch (final Exception e) {
        }
    }

    @Test
    public void add() {
        service.add(USER1);
        service.add(USER2);
        compareUsers(USER1, service.findOneById(USER1.getId()));
        compareUsers(USER2, service.findOneById(USER2.getId()));
    }

    @Test
    public void addList() {
        service.add(USER_LIST1);
        compareUsers(USER1, service.findOneById(USER1.getId()));
        compareUsers(USER2, service.findOneById(USER2.getId()));
        compareUsers(ADMIN3, service.findOneById(ADMIN3.getId()));
    }

    @Test
    public void setList() {
        service.add(USER_LIST1);
        service.set(USER_LIST2);
        compareUsers(USER4, service.findOneById(USER4.getId()));
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void clear() {
        service.add(USER_LIST1);
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void existsById() {
        service.add(USER_LIST1);
        Assert.assertTrue(service.existsById(USER1.getId()));
        Assert.assertTrue(service.existsById(USER2.getId()));
        Assert.assertTrue(service.existsById(ADMIN3.getId()));
        Assert.assertFalse(service.existsById(USER4.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER_LIST1);
        compareUsers(USER1, service.findOneById(USER1.getId()));
        compareUsers(USER2, service.findOneById(USER2.getId()));
        compareUsers(ADMIN3, service.findOneById(ADMIN3.getId()));
        thrown.expect(EntityNotFoundException.class);
        compareUsers(USER4, service.findOneById(USER4.getId()));
    }

    @Test
    public void remove() {
        service.add(USER_LIST1);
        service.add(USER4);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER2.getId());
        Assert.assertEquals(2, service.getSize());
        compareUsers(ADMIN3, service.findAll().get(0));
        compareUsers(USER4, service.findAll().get(1));
        service.clear();
        Assert.assertEquals(0, service.getSize());
        service.add(USER1);
        service.add(USER2);
        projectService.add(USER1_PROJECT1);
        taskService.add(USER1_TASK1);
        projectService.add(USER2_PROJECT1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(2, projectService.getSize());
        Assert.assertEquals(2, taskService.getSize());
        service.removeByLogin("user1");
        Assert.assertEquals(1, projectService.getSize());
        compareProjects(USER2_PROJECT1, projectService.findAll().get(0));
        Assert.assertEquals(1, taskService.getSize());
        compareTasks(USER2_TASK1, taskService.findAll().get(0));
        service.removeByEmail("user2@gmail.com");
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void createWithEmail() {
        Assert.assertTrue(service.findAll().isEmpty());
        service.add(USER1);
        @NotNull final UserDTO user = service.create("user4", "password2", "email4@gmail.com");
        Assert.assertEquals(2, service.getSize());
        Assert.assertEquals("user4", service.findAll().get(1).getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "password2"), user.getPasswordHash());
        Assert.assertEquals("email4@gmail.com", service.findAll().get(1).getEmail());
        thrown.expect(UserLoginExistsException.class);
        service.create("user1", "password1", "email1@gmail.com");
    }

    @Test
    public void createWithRole() {
        Assert.assertTrue(service.findAll().isEmpty());
        service.add(USER1);
        @NotNull final UserDTO user = service.create("user4", "password2", Role.USUAL);
        Assert.assertEquals(2, service.getSize());
        Assert.assertEquals("user4", service.findAll().get(1).getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "password2"), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, service.findAll().get(1).getRole());
    }

    @Test
    public void findByLogin() {
        service.add(USER_LIST1);
        compareUsers(USER1, service.findByLogin("user1"));
    }

    @Test
    public void findByEmail() {
        service.add(USER_LIST1);
        compareUsers(USER1, service.findByEmail("user1@gmail.com"));
    }

    @Test
    public void setPassword() {
        @NotNull UserDTO user = service.create("test", "password");
        service.setPassword(user.getId(), "new_password");
        Assert.assertEquals(HashUtil.salt(propertyService, "new_password"),
                service.findByLogin("test").getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull UserDTO user = service.create("test", "password");
        service.updateUser(user.getId(), "fstName", "lastName", "midName");
        @NotNull final UserDTO foundUser = service.findByLogin("test");
        Assert.assertEquals("fstName", foundUser.getFirstName());
        Assert.assertEquals("lastName", foundUser.getLastName());
        Assert.assertEquals("midName", foundUser.getMiddleName());
    }

    @Test
    public void isLoginExist() {
        Assert.assertFalse(service.isLoginExist("user1"));
        service.add(USER1);
        Assert.assertTrue(service.isLoginExist("user1"));
    }

    @Test
    public void isEmailExist() {
        Assert.assertFalse(service.isEmailExist("user1@gmail.com"));
        service.add(USER1);
        Assert.assertTrue(service.isEmailExist("user1@gmail.com"));
    }

    @Test
    public void lockUserByLogin() {
        service.add(USER1);
        Assert.assertFalse(USER1.getLocked());
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(service.findByLogin(USER1.getLogin()).getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        service.add(USER1);
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(service.findByLogin(USER1.getLogin()).getLocked());
        service.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(service.findByLogin(USER1.getLogin()).getLocked());
    }

}
*/
