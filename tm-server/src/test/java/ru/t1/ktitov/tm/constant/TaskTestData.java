package ru.t1.ktitov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

public final class TaskTestData {

    @NotNull
    public final static TaskDTO USER1_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER1_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER1_TASK3 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER2_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN1_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN1_TASK2 = new TaskDTO();

    @NotNull
    public final static List<TaskDTO> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2, USER1_TASK3);

    @NotNull
    public final static List<TaskDTO> USER2_TASK_LIST = Collections.singletonList(USER2_TASK1);

    @NotNull
    public final static List<TaskDTO> ADMIN1_TASK_LIST = Arrays.asList(ADMIN1_TASK1, ADMIN1_TASK2);

    @NotNull
    public final static List<TaskDTO> USER1USER2_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2,
            USER1_TASK3, USER2_TASK1);

    @NotNull
    public final static List<TaskDTO> TASK_LIST = new ArrayList<>();

    static {
        USER1_TASK_LIST.forEach(task -> task.setUserId(USER1.getId()));
        USER2_TASK_LIST.forEach(task -> task.setUserId(USER2.getId()));
        ADMIN1_TASK_LIST.forEach(task -> task.setUserId(ADMIN3.getId()));

        USER1_TASK_LIST.forEach(task -> task.setProjectId(USER1_PROJECT1.getId()));
        USER2_TASK_LIST.forEach(task -> task.setProjectId(USER2_PROJECT1.getId()));

        TASK_LIST.addAll(USER1_TASK_LIST);
        TASK_LIST.addAll(USER2_TASK_LIST);
        TASK_LIST.addAll(ADMIN1_TASK_LIST);

        for (int i = 0; i < TASK_LIST.size(); i++) {
            @NotNull final TaskDTO task = TASK_LIST.get(i);
            task.setId("t-0" + i);
            task.setName("task-" + i);
            task.setDescription("description of task " + i);
        }
    }

}
