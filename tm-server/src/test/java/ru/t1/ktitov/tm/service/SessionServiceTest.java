/*
package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.ISessionService;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.service.dto.SessionService;

import java.util.List;

import static ru.t1.ktitov.tm.constant.SessionTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    private void compareSessions(@NotNull final SessionDTO session1, @NotNull final SessionDTO session2) {
        Assert.assertEquals(session1.getId(), session2.getId());
        Assert.assertEquals(session1.getUserId(), session2.getUserId());
        Assert.assertEquals(session1.getRole(), session2.getRole());
    }

    private void compareSessions(
            @NotNull final List<SessionDTO> sessionList1,
            @NotNull final List<SessionDTO> sessionList2) {
        Assert.assertEquals(sessionList1.size(), sessionList2.size());
        for (int i = 0; i < sessionList1.size(); i++) {
            compareSessions(sessionList1.get(i), sessionList2.get(i));
        }
    }

    @After
    public void tearDown() {
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER1_SESSION1);
        service.add(USER1_SESSION2);
        compareSessions(USER1_SESSION1, service.findAll().get(0));
        compareSessions(USER1_SESSION2, service.findAll().get(1));
    }

    @Test
    public void addByUserId() {
        service.add(USER1.getId(), USER1_SESSION1);
        compareSessions(USER1_SESSION1, service.findAll().get(0));
        Assert.assertEquals(USER1.getId(), service.findAll().get(0).getUserId());
    }

    @Test
    public void addList() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.getSize());
        compareSessions(USER1_SESSION1, service.findAll().get(0));
        compareSessions(USER1_SESSION2, service.findAll().get(1));
        compareSessions(USER1_SESSION3, service.findAll().get(2));
    }

    @Test
    public void setList() {
        service.add(USER1_SESSION_LIST);
        service.set(USER2_SESSION_LIST);
        compareSessions(USER2_SESSION1, service.findAll().get(0));
        compareSessions(USER2_SESSION2, service.findAll().get(1));
        compareSessions(USER2_SESSION3, service.findAll().get(2));
    }

    @Test
    public void clear() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
        service.add(USER1_SESSION_LIST);
        service.add(USER2_SESSION_LIST);
        service.clear(USER1.getId());
        Assert.assertEquals(3, service.getSize());
        compareSessions(USER2_SESSION1, service.findAll().get(0));
        compareSessions(USER2_SESSION2, service.findAll().get(1));
        compareSessions(USER2_SESSION3, service.findAll().get(2));
    }

    @Test
    public void findAllByUserId() {
        service.add(USER1_SESSION_LIST);
        service.add(USER2_SESSION_LIST);
        compareSessions(USER1_SESSION_LIST, service.findAll(USER1.getId()));
        compareSessions(USER2_SESSION_LIST, service.findAll(USER2.getId()));
    }

    @Test
    public void existsById() {
        service.add(USER1_SESSION1);
        service.add(USER2_SESSION1);
        Assert.assertTrue(service.existsById(USER1_SESSION1.getId()));
        Assert.assertTrue(service.existsById(USER2_SESSION1.getId()));
        Assert.assertFalse(service.existsById(USER1_SESSION2.getId()));
        Assert.assertTrue(service.existsById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(service.existsById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER1_SESSION1);
        service.add(USER2_SESSION1);
        compareSessions(USER1_SESSION1, service.findOneById(USER1_SESSION1.getId()));
        compareSessions(USER2_SESSION1, service.findOneById(USER2_SESSION1.getId()));
        compareSessions(USER1_SESSION1, service.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        thrown.expect(EntityNotFoundException.class);
        compareSessions(USER1_SESSION2, service.findOneById(USER1_SESSION2.getId()));
        compareSessions(USER2_SESSION1, service.findOneById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void remove() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.getSize());
        service.remove(USER1_SESSION1);
        Assert.assertEquals(2, service.getSize());
        service.removeById(USER1_SESSION2.getId());
        Assert.assertEquals(1, service.getSize());
        compareSessions(USER1_SESSION3, service.findAll().get(0));
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeByUserId() {
        service.add(USER1_SESSION_LIST);
        Assert.assertEquals(3, service.getSize());
        service.remove(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(2, service.getSize());
        service.removeById(USER1.getId(), USER1_SESSION2.getId());
        Assert.assertEquals(1, service.getSize());
        compareSessions(USER1_SESSION3, service.findAll().get(0));
    }

}
*/
